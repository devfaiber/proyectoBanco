import wx
from models.settings import getConexion
from models.migrate import crearDB, crearADMIN
from sqlalchemy.exc import InternalError
from sqlalchemy import create_engine, Column
from sqlalchemy import Integer, ForeignKey, String, Float, DECIMAL, Boolean, Date, DateTime
from sqlalchemy.ext.declarative import declarative_base
import datetime
engine = create_engine(getConexion())
baseDB = declarative_base(engine)
metadata = baseDB.metadata



class User(baseDB):

    __tablename__ = "usuarios"
    id = Column(Integer, primary_key=True, autoincrement=True)
    username = Column(String(30), unique=True)
    password = Column(String(30))
    rol = Column(String(30))
    estado = Column(Boolean)

    def __repr__(self):
        return "<Users(username={},password={},rol={},estado={})>".format(self.username, self.password, self.rol, self.estado)


class Cliente(baseDB):
    __tablename__ = "clientes"
    id = Column(Integer, primary_key=True, autoincrement=True)
    dni = Column(String(30), unique=True)
    nombre = Column(String(30))
    apellidos = Column(String(30))
    direccion = Column(String(50))
    email = Column(String(30), unique=True)
    telefono = Column(String(30))

    def __repr__(self):
        return "<clientes(dni={},nombre={},apellidos={},direccion={},email={},telefono={})>".format(self.dni, self.nombre, self.apellidos, self.direccion, self.email, self.telefono)

class Cuenta(baseDB):
    __tablename__ = "cuentas"
    id = Column(Integer, primary_key=True, autoincrement=True)
    tipo = Column(String(5))
    numero = Column(String(15), unique=True)
    saldo = Column(DECIMAL(11, 3))
    interes = Column(Float)
    comision = Column(Float)
    puntos = Column(Integer)
    fecha_creada = Column(Date, default=datetime.date.today())
    estado = Column(String(15))
    id_cliente = Column(Integer, ForeignKey("clientes.id"))

    def __repr__(self):
        return "<cuentas(tipo={},numero={},saldo={},interes={},comision={},puntos={},estado={},id_cliente={})>".format(self.tipo,self.numero,self.saldo,self.interes,self.comision,self.puntos,self.estado,self.id_cliente)

class Movimiento(baseDB):
    __tablename__ = "movimientos"
    id = Column(Integer, primary_key=True, autoincrement=True)
    id_cuenta = Column(Integer, ForeignKey("cuentas.id"))
    tipo = Column(String(10))
    dinero = Column(DECIMAL(11, 3))
    fecha = Column(DateTime, default=datetime.datetime.now())
    estado = Column(Boolean, default=True)

    def __repr__(self):
        return "<Movimientos(tipo={},id_cuenta={},dinero={},estado={})>".format(self.tipo, self.id_cuenta,self.dinero,self.estado)


try:
    metadata.create_all()
except InternalError:
    crearDB()
    metadata.create_all()
    crearADMIN()


