from models.settings import getConexion
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from models.model import Movimiento
from sqlalchemy.orm.exc import NoResultFound

class MovimientoModel():
    def conectarDB(self):
        engine = create_engine(getConexion())
        Session = sessionmaker(engine)
        session = Session()
        return session

    def buscar(self, **filtro):
        session = self.conectarDB()
        try:
            movimiento = session.query(Movimiento).filter_by(**filtro).all()
            session.close()

            if movimiento:
                return True
            else:
                return False


        except NoResultFound:
            session.close()
            return False

    def registrar(self, datos):
        session = self.conectarDB()
        movimiento = Movimiento(**datos)
        session.add(movimiento)
        session.commit()
        session.close()

        return True

    def getMovimiento(self, **filtro):
        session = self.conectarDB()
        movimiento = session.query(Movimiento).filter_by(**filtro).all()

        session.close()

        return movimiento

    def deleteMovimiento(self, **id):
        if self.buscar(**id):
            session = self.conectarDB()
            movimiento = session.query(Movimiento).filter_by(**id).delete()
            session.commit()
            session.close()

            if movimiento:
                return True
            else:
                return False

        else:
            return False
