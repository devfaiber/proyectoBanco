from models.settings import getConexion

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from models.model import Cliente

from sqlalchemy.orm.exc import MultipleResultsFound, NoResultFound
from sqlalchemy.exc import IntegrityError

class ClienteModel():
    def __init__(self):
        pass

    def conectarDB(self):
        engine = create_engine(getConexion())
        Session = sessionmaker(engine)
        session = Session()
        return session


    def buscar(self, **filtro):
        session = self.conectarDB()
        cliente = session.query(Cliente).filter_by(**filtro).one_or_none()
        session.close()

        if cliente is None:
            return False
        else:
            return True

    def crear(self, datos):
        session = self.conectarDB()
        try:
            #mayuscula
            datos["dni"] = datos["dni"].upper()

            cliente = Cliente(**datos)
            session.add(cliente)
            session.commit()
            session.close()

            return True
        #si el email ya esta en la db
        except IntegrityError:
            session.close()
            return False



    def editar(self, id, datos):
        session = self.conectarDB()
        try:

            cliente = session.query(Cliente).filter_by(id=id).one()

            cliente.dni = datos["dni"].upper()
            cliente.nombre = datos["nombre"]
            cliente.apellidos = datos["apellidos"]
            cliente.direccion = datos["direccion"]
            cliente.email = datos["email"]
            cliente.telefono = datos["telefono"]

            session.add(cliente)
            session.commit()
            session.close()

            return True
        except IntegrityError:
            session.close()
            return False

    def delete(self, id):
        session = self.conectarDB()
        try:

            cliente = session.query(Cliente).filter_by(id=id).one()
            session.delete(cliente)
            session.commit()
            session.close()
            return True
        except (NoResultFound, MultipleResultsFound):
            session.close()
            return False
        except IntegrityError:
            return False


    def getAll(self):
        session = self.conectarDB()
        result = session.query(Cliente).all()
        session.close()

        return result


    def getOne(self, id):
        session = self.conectarDB()
        try:
            result = session.query(Cliente).filter_by(id=id).one()
            session.close()
            return result

        except (NoResultFound, MultipleResultsFound):
            session.close()
            return None
