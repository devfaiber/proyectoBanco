import pymysql

from time import sleep
from models.settings import HOST_DB, USER_DB, PWD_DB, NAME_DB
from models.settings import ROOT_USERNAME,ROOT_PASSWORD,ROOT_ROLE,ROOT_STATUS
def crearDataBase():


    try:
        conectar = pymysql.connect(host=HOST_DB, user=USER_DB, password=PWD_DB)

        cursor = conectar.cursor()
        sql = "CREATE DATABASE {}".format(NAME_DB)
        cursor.execute(sql)
        cursor.close()
        conectar.close()
        return True
    except:
        return False


def crearDB():
    print("====================================")
    print("======>=>=>   GESTOR DE ERRORES")
    print("====================================")
    if crearDataBase():

        print("=>   la base de datos se esta generando porfavor espere ...")
        sleep(5)
        print("=>   la base de datos se creo exitosamente")
        print("-----------------------------")

    else:
        print("=>   Esta base de datos es imposible de crear porfavor le toco crearla manual")



def crearADMIN():

    conectar = pymysql.connect(host=HOST_DB, user=USER_DB, password=PWD_DB, db=NAME_DB)
    cursor = conectar.cursor()
    sql = "INSERT INTO usuarios(username,password,rol,estado)VALUES ('%s','%s','%s',%s)"%(ROOT_USERNAME,ROOT_PASSWORD,ROOT_ROLE,ROOT_STATUS)
    cursor.execute(sql)

    cursor.close()
    conectar.commit()
    conectar.close()

    print("=>   iniciando proceso de creacion de usuario administrador ...")
    sleep(4)
    print("=>   creando usuario administrador principal finalizando ...")
    sleep(3)
    print("=>   el usuario se ha creado exitosamente")
    print("[username] => admin")
    print("[password] => admin123")
    print("[status] => activo")
    sleep(2)

    return True




    print("=========<<<     FIN DEL SISTEMA     >>>=========")
