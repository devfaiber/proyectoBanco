from models.settings import getConexion

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from models.model import Cuenta
from models.movimientoModel import MovimientoModel

from sqlalchemy.orm.exc import MultipleResultsFound, NoResultFound
from sqlalchemy.exc import IntegrityError
import datetime

class CuentaModel():

    def conectarDB(self):
        engine = create_engine(getConexion())
        Session = sessionmaker(engine)
        session = Session()
        return session

    def buscar(self, **filtro):
        session = self.conectarDB()
        busqueda = session.query(Cuenta).filter_by(**filtro).one_or_none()

        if busqueda is None:
            return False
        else:
            return True

    def crear(self, datos):
        session = self.conectarDB()
        try:
            cuenta = Cuenta(**datos)
            session.add(cuenta)
            session.commit()
            session.close()
            return True
        #si el numero o el id_cliente no son correcto
        except IntegrityError:
            session.close()
            return False

    def editar(self, id, datos):
        session = self.conectarDB()
        try:
            cuenta = session.query(Cuenta).filter_by(id=id).one()
            cuenta.tipo = datos["tipo"]
            cuenta.numero = datos["numero"]
            cuenta.saldo = datos["saldo"]
            cuenta.interes = datos["interes"]
            cuenta.comision = datos["comision"]
            cuenta.puntos = datos["puntos"]
            cuenta.estado = datos["estado"]
            cuenta.id_cliente = datos["id_cliente"]

            session.add(cuenta)
            session.commit()
            session.close()

            return True
        except (NoResultFound, MultipleResultsFound):
            return 404 # 3 indica error
        except IntegrityError:
            return False

    def getAll(self):
        session = self.conectarDB()
        resultado = session.query(Cuenta).all()
        session.close()

        return resultado

    def getOne(self, **dato):
        if self.buscar(**dato):
            session = self.conectarDB()
            resultado = session.query(Cuenta).filter_by(**dato).one()
            session.close()

            return resultado
        else:
            return None

    def delete(self, id):
        if self.buscar(id=id):
            self.deleteMovimientos(id)
            session = self.conectarDB()

            try:
                cuenta = session.query(Cuenta).filter_by(id=id).one()
                session.delete(cuenta)
                session.commit()
                session.close()
                return True
            except IntegrityError:
                session.close()
                print("Error de foranea")
                return False

        else:
            return False

    def consignarSaldo(self, id, saldo, puntos=None):
        if self.buscar(id=id):
            session = self.conectarDB()
            cuenta = session.query(Cuenta).filter_by(id=id).one()
            cuenta.saldo = float(cuenta.saldo) + float(saldo)
            cuenta.puntos = cuenta.puntos + puntos

            session.add(cuenta)
            session.commit()
            session.close()

            return True
        else:
            return False

    def retirarSaldo(self, id, saldo):
        if self.buscar(id=id):
            session = self.conectarDB()
            cuenta = session.query(Cuenta).filter_by(id=id).one()
            cuenta.saldo = float(cuenta.saldo) - float(saldo)

            session.add(cuenta)
            session.commit()
            session.close()

            return True
        else:
            return False

    def deleteMovimientos(self, id):
        movimiento = MovimientoModel()

        return movimiento.deleteMovimiento(id_cuenta=id)

    def bloquearCuenta(self,id, estado="bloqueada"):
        session = self.conectarDB()
        try:
            cuenta = session.query(Cuenta).filter_by(id=id).one()
            cuenta.estado = estado

            session.add(cuenta)
            session.commit()
            session.close()

            return True
        except (NoResultFound, MultipleResultsFound):
            return 404  # 3 indica error
        except IntegrityError:
            return False
