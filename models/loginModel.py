from models.settings import getConexion

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.exc import MultipleResultsFound
from sqlalchemy.exc import IntegrityError
from models.model import User


class LoginModel():

    def conectarDB(self):
        engine = create_engine(getConexion())
        Session = sessionmaker(engine)
        session = Session()

        return session

    def getLoguin(self, username, password):
        if self.buscar(username, password):
            session = self.conectarDB()
            result = session.query(User).filter_by(username=username, password=password).one()
            session.close()

            return result
        else:
            return None

    def buscar(self, username, password):
        try:
            session = self.conectarDB()
            resultado = session.query(User).filter_by(username=username, password=password).one_or_none()
            session.close()

            if resultado is None:
                return False
            else:
                return True

        except MultipleResultsFound:
            return True

    def crear(self, datos):
        session = self.conectarDB()
        try:
            user = User(**datos)
            session.add(user)
            session.commit()
            session.close()
            return True
        except IntegrityError:
            session.close()
            return False
