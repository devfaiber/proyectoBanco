from models.clienteModel import ClienteModel
import wx
import re
from wx import xrc

EXPRESION_REGULAR_EMAIL = r'[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(\.[a-z]{2,4})$'
EXPRESION_REGULAR_TELEFONO = r'([0-9]{8,12})$'
EXPRESION_REGULAR_DNI = r'([0-9]{8})+([a-zA-Z]{1})$'

class EditarCliente(wx.Frame):

    def __init__(self, moduloCliente, id):
        super(EditarCliente, self).__init__()

        self.idCliente = id
        self.moduloCliente = moduloCliente
        self.clienteModel = ClienteModel()

        self.resp = xrc.XmlResource("../views/editar_cliente.xrc")
        self.frame = self.resp.LoadFrame(None, "editClientFrame")
        self.panel = xrc.XRCCTRL(self.frame, "panelEditCliente")

        self.dni = xrc.XRCCTRL(self.panel, "dni")
        self.nombre = xrc.XRCCTRL(self.panel, "nombre")
        self.apellidos = xrc.XRCCTRL(self.panel, "apellidos")
        self.direccion = xrc.XRCCTRL(self.panel, "direccion")
        self.telefono = xrc.XRCCTRL(self.panel, "telefono")
        self.email = xrc.XRCCTRL(self.panel, "email")
        self.btnRegistrar = xrc.XRCCTRL(self.panel, "btnEditClientes")
        self.frame.Bind(wx.EVT_BUTTON, self.actualizarCliente, self.btnRegistrar)
        self.loadDatosCliente()

        self.frame.Show()


    def getDatos(self):
        datos = {
            "dni": self.dni.GetValue(),
            "nombre": self.nombre.GetValue(),
            "apellidos": self.apellidos.GetValue(),
            "direccion": self.direccion.GetValue(),
            "telefono": self.telefono.GetValue(),
            "email": self.email.GetValue()
        }
        return datos


    def loadDatosCliente(self):
        resultado = self.clienteModel.getOne(self.idCliente)
        if not resultado is None:

            self.dni.SetValue(resultado.dni)
            self.nombre.SetValue(resultado.nombre)
            self.apellidos.SetValue(resultado.apellidos)
            self.direccion.SetValue(resultado.direccion)
            self.telefono.SetValue(resultado.telefono)
            self.email.SetValue(resultado.email)
        else:
            self.frame.Close()


    def actualizarCliente(self, evento):
        datos = self.getDatos()

        if not self.validarDatos(datos):
            return False

        resultado = self.clienteModel.editar(self.idCliente, datos)

        if resultado:
            wx.MessageBox("el cliente fue modificado exitosamente","CLIENTE EDITADO",wx.OK|wx.ICON_INFORMATION)
            self.moduloCliente.cargarDatosColumnas()
            self.frame.Close()
        else:
            wx.MessageBox("Lo sentimos el cliente no se pudo actualizar intente mas tarde", "ERROR DE ACTUALIZACION", wx.OK | wx.ICON_INFORMATION)


    def validarDatos(self, datos):
        datosDB = self.clienteModel.getOne(id=self.idCliente)
        estado = True

        if datos["dni"] == "" or datos["nombre"] == "" or datos["apellidos"] == "" or datos["email"] == "":
            wx.MessageBox("faltan campos requeridos(*) por rellenar. porfavor rellene los campos faltantes","CAMPOS REQUERIDOS VACIOS", wx.OK | wx.ICON_WARNING)
            estado = False

        elif not self.isValidoDNI(dni=datos["dni"]):
            estado = False

        elif datos["telefono"] != "" and not re.match(EXPRESION_REGULAR_TELEFONO, datos["telefono"]):
            wx.MessageBox("El numero de telefono debe ser numerico y maximo de 8 a 12 digitos", "ERROR DE TELEFONO", wx.OK | wx.ICON_ERROR)
            estado = False

        elif not re.match(EXPRESION_REGULAR_EMAIL,datos["email"]):
            wx.MessageBox("El email invalido, coloca un email que sea valido", "ERROR EMAIL", wx.OK | wx.ICON_ERROR)
            estado = False

        elif self.clienteModel.buscar(dni=datos["dni"]) and datosDB.dni != datos["dni"]:
            wx.MessageBox("El DNI ya existe en la base de datos","ERROR DE DNI", wx.OK|wx.ICON_ERROR)
            estado = False

        elif self.clienteModel.buscar(email=datos["email"]) and datosDB.email != datos["email"]:
            wx.MessageBox("El email ya existe en la base de datos", "ERROR DE EMAIL", wx.OK | wx.ICON_ERROR)
            estado = False

        return estado

    def isValidoDNI(self, dni):

        if not re.match(EXPRESION_REGULAR_DNI,dni):
            wx.MessageBox("El DNI debe posee un total de 8 digitos y una letra al final","ERROR DE DNI",wx.OK|wx.ICON_ERROR)
            return False
        else:
            return True


