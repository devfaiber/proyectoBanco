import wx
from wx import xrc
from models.cuentaModel import CuentaModel
from models.movimientoModel import MovimientoModel

EURO_FOR_PUNTOS = 6
class DepositoFrame(wx.Frame):
    def __init__(self, moduloCuenta,id):
        super(DepositoFrame, self).__init__()

        self.idCuenta = id
        self.moduloCuenta = moduloCuenta
        self.cuentaModel = CuentaModel()
        self.movimientoModel = MovimientoModel()

        self.recurso = xrc.XmlResource("../views/depositar_cuenta.xrc")
        self.frame = self.recurso.LoadFrame(None, "depositarFrame")

        self.panel = xrc.XRCCTRL(self.frame, "panel_depositar")
        self.campoNumero = xrc.XRCCTRL(self.panel, "numero")
        self.campoDinero = xrc.XRCCTRL(self.panel, "cantidad_dinero")
        self.btnDepositar = xrc.XRCCTRL(self.panel, "btn_depositar")
        self.frame.Bind(wx.EVT_BUTTON, self.depositar, self.btnDepositar)

        self.cargarNumeroCuenta()

        self.frame.Show()

    def cargarNumeroCuenta(self):
        resultado = self.cuentaModel.getOne(id=self.idCuenta)
        euroDefault = 1

        self.campoNumero.SetValue(resultado.numero)
        self.campoDinero.SetValue(str(euroDefault))


    def depositar(self, event):
        try:
            if float(self.campoDinero.GetValue()) > 0:
                respuesta = wx.MessageBox("Deseas continuar la operacion", "CONFIRMACION", wx.YES_NO | wx.ICON_EXCLAMATION)

                if respuesta == wx.YES:
                    self.validarDeposito()
            else:
                wx.MessageBox("Porfavor introduza una cantidad minimo 1 €", "ERROR DE DEPOSITO",wx.OK|wx.ICON_ERROR)
        except ValueError:
            wx.MessageBox("Este campo solo administe Numeros", "ERROR DE VALOR", wx.OK | wx.ICON_ERROR)

    def validarDeposito(self):
        saldo = self.campoDinero.GetValue()
        puntos = self.getPuntos(saldo)

        self.quitarBloqueo(saldo)
        result = self.cuentaModel.consignarSaldo(self.idCuenta, saldo, puntos)

        if result:
            self.registrarDeposito(tipo="deposito", dinero=saldo)

            wx.MessageBox("su consignacion fue procesada con exito", "CONSIGNACION EXITOSA",wx.OK | wx.ICON_INFORMATION)
            self.moduloCuenta.cargarDatosColumnas()

            self.frame.Close()

        else:
            wx.MessageBox("lo sentimos no se ha procesado su consignacion, intente mas tarde", "ERROR DE CONSIGNACION",wx.OK | wx.ICON_INFORMATION)


    def quitarBloqueo(self,deposito):
        resultado = self.cuentaModel.getOne(id=self.idCuenta)

        if resultado.estado == "bloqueada":

            if float(deposito) > abs(float(resultado.saldo)):
                print("se levanto el bloqueo")
                self.cuentaModel.bloquearCuenta(id=self.idCuenta, estado="activa")
            else:
                print("el bloqueo continua hasta que pagues lo que falta")



    def getPuntos(self, saldo):
        puntos = int(float(saldo) / EURO_FOR_PUNTOS)

        return puntos


    def registrarDeposito(self,**params):
        datos = {
            "id_cuenta": self.idCuenta,
            "tipo": params["tipo"],
            "dinero": params["dinero"],
            "estado": True
        }

        resultado = self.movimientoModel.registrar(datos)

        if resultado:
            print("el movimiento fue registrado")
        else:
            print("el movimiento no pudo ser registrado")