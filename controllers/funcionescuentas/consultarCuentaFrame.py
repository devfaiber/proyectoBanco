import wx
from wx import xrc
from models.cuentaModel import CuentaModel

class ConsultarFrame(wx.Frame):
    def __init__(self, id):
        super(ConsultarFrame, self).__init__()
        self.listaCuentas = {"FI":"Fondo de inversion", "CC":"Cuenta Corriente", "CV":"Cuenta Vivienda"}
        self.idCuenta = id
        self.cuentaModel = CuentaModel()

        self.recurso = xrc.XmlResource("../views/consultar_cuenta.xrc")
        self.frame = self.recurso.LoadFrame(None, "consultarCuentaFrame")
        self.panel = xrc.XRCCTRL(self.frame, "panel_consultar")

        self.saldo = xrc.XRCCTRL(self.panel, "saldo_texto")
        self.interes = xrc.XRCCTRL(self.panel, "interes_texto")
        self.comision = xrc.XRCCTRL(self.panel, "comision_texto")
        self.numero_cuenta = xrc.XRCCTRL(self.panel, "numero_texto")
        self.tipo_cuenta = xrc.XRCCTRL(self.panel, "tipo_texto")
        self.estado = xrc.XRCCTRL(self.panel, "estado_texto")
        self.puntos = xrc.XRCCTRL(self.panel, "puntos_texto")

        self.frame.Show()

    def cargarDatosCuentas(self):
        result = self.cuentaModel.getOne(id=self.idCuenta)

        if result is None:
            wx.MessageBox("Lo sentimos cuenta no encontrada intente mas tarde", "ERROR DE CUENTA INEXISTENTE",wx.OK|wx.ICON_ERROR)
            self.frame.Close()
        else:
            self.setDatosCampos(result)

    def setDatosCampos(self, datos):
        self.saldo.SetLabel(str(datos.saldo))
        self.interes.SetLabel(str(int(datos.interes*100))+"%")
        self.comision.SetLabel(str(int(datos.comision*100))+"%")
        self.numero_cuenta.SetLabel(datos.numero)
        self.tipo_cuenta.SetLabel(self.listaCuentas[datos.tipo])
        self.estado.SetLabel(datos.estado)
        self.puntos.SetLabel(str(datos.puntos))