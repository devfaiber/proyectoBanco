import wx
from wx import xrc
from models.cuentaModel import CuentaModel
from models.movimientoModel import MovimientoModel
import re


DINERO_MINIMO_FI = -500

FONDO_INVERSION = "FI"
CUENTA_CORRIENTE = "CC"
CUENTA_VIVIENDA = "CV"

EXPRESION_NUMERO = r'([0-9]+|([0-9]+)+(\.[0-9]{1,3}))$'

class RetiroFrame(wx.Frame):
    def __init__(self, moduloCuenta, id):
        super(RetiroFrame, self).__init__()
        self.saldoPermitido = None
        self.saldoTotal = None
        self.dineroSacar = None

        self.idCuenta = id
        self.moduloCuenta = moduloCuenta
        self.cuentaModel = CuentaModel()
        self.movimientoModel = MovimientoModel()

        self.recurso = xrc.XmlResource("../views/retirar_cuenta.xrc")
        self.frame = self.recurso.LoadFrame(None, "retirarFrame")

        self.panel = xrc.XRCCTRL(self.frame, "panel_retirar")
        self.campoNumero = xrc.XRCCTRL(self.panel, "numero")
        self.campoDinero = xrc.XRCCTRL(self.panel, "cantidad_dinero")
        self.btnRetirar = xrc.XRCCTRL(self.panel, "btn_retirar")
        self.panel.Bind(wx.EVT_BUTTON, self.validarCampo, self.btnRetirar)

        self.txtTotalDinero = xrc.XRCCTRL(self.panel, "total_dinero")
        self.txtCantidadPermitida = xrc.XRCCTRL(self.panel, "catidad_permitida")


        self.setData()

        self.frame.Show()

    def validarCampo(self, evento):
        if re.match(EXPRESION_NUMERO, self.campoDinero.GetValue()):
            self.retirar(evento)
            return False
        wx.MessageBox("lo sentimos no se admiten valores extraños ni cadenas de texto, intente mas tarde", "TRANSACCION RETENIDA",wx.OK | wx.ICON_INFORMATION)



    def setData(self):
        resultado = self.cuentaModel.getOne(id=self.idCuenta)
        self.saldoTotal = float(resultado.saldo)

        self.campoNumero.SetValue(resultado.numero)
        self.txtTotalDinero.SetLabel(str(resultado.saldo))

        self.setDataPermitido(resultado)

    def setDataPermitido(self, datos):

        if datos.tipo == CUENTA_CORRIENTE:
            self.saldoPermitido = self.saldoTotal

        elif datos.tipo == CUENTA_VIVIENDA:
            self.saldoPermitido = None

        elif datos.tipo == FONDO_INVERSION:
            if self.saldoTotal >= DINERO_MINIMO_FI:
               self.saldoPermitido = self.saldoTotal + (DINERO_MINIMO_FI * -1)

            else:
                self.saldoPermitido = 0.00

        self.txtCantidadPermitida.SetLabel(str(self.saldoPermitido))

    def retirar(self, evento):
        self.dineroSacar = float(self.campoDinero.GetValue())

        respuesta = wx.MessageBox("Deseas continuar con esta operacion, verifica tu saldo?", "VERIFICACION", wx.YES_NO | wx.ICON_EXCLAMATION)

        if respuesta == wx.YES:
            self.validarRetiro()

    def validarRetiro(self):
        datosDB = self.cuentaModel.getOne(id=self.idCuenta)
        if datosDB.tipo == FONDO_INVERSION:
            if self.validarRetiroFI():
                wx.MessageBox("su retiro fue exitoso no olvides la tarjeta", "TRANSACCION EXITOSA", wx.OK | wx.ICON_INFORMATION)
                self.registrarRetiro(tipo="Retiro", dinero=self.dineroSacar)


        elif datosDB.tipo == CUENTA_VIVIENDA:

            wx.MessageBox("Lo sentimos no pueder retirar de una cuenta de vivienda","ERROR DE RETIRO", wx.OK | wx.ICON_ERROR)

        elif datosDB.tipo == CUENTA_CORRIENTE:

            if self.validarRetiroCC():
                wx.MessageBox("su retiro fue exitoso no olvides la tarjeta", "TRANSACCION EXITOSA", wx.OK | wx.ICON_INFORMATION)
                self.registrarRetiro(tipo="Retiro", dinero=self.dineroSacar)


        self.frame.Close()

    def validarRetiroCC(self):
        if self.dineroSacar < 0:

            wx.MessageBox("Lo sentimos no puedes sacar saldo negativo", "ERROR DE RETIRO", wx.OK | wx.ICON_ERROR)
            return False
        elif self.dineroSacar > self.saldoTotal:

            wx.MessageBox("Lo sentimos la cantidad que vas a sacar sobrepasa lo de tu cuenta", "ERROR DE RETIRO", wx.OK | wx.ICON_ERROR)
            return False

        return True


    def validarRetiroFI(self):
        saldoRestadoRetiro = self.saldoTotal
        retiro = self.dineroSacar
        saldoRestadoRetiro -= retiro


        if retiro < 0:
            wx.MessageBox("El retiro no puede ser negativo","ERROR DE RETIRO",wx.OK|wx.ICON_ERROR)
            return False

        elif self.saldoTotal <= DINERO_MINIMO_FI:
            wx.MessageBox("El retiro no se proceso, cuenta bloqueda por intentar sacar por debajo de -500. paque su tarjeta ", "ERROR DE RETIRO", wx.OK | wx.ICON_ERROR)
            self.cuentaModel.bloquearCuenta(id=self.idCuenta)
            self.registrarRetiro(tipo="Retiro", dinero=self.dineroSacar, estado=False)

            return False

        elif retiro > self.saldoPermitido:
            wx.MessageBox("El retiro es mayor al saldo permitido","ERROR DE RETIRO",wx.OK|wx.ICON_ERROR)
            return False

        elif saldoRestadoRetiro >= DINERO_MINIMO_FI:
            return True
        else:
            wx.MessageBox("El retiro no puede ser procesado intente mas tarde", "ERROR DE RETIRO", wx.OK | wx.ICON_ERROR)
            return False


    def registrarRetiro(self,tipo="Retiro",dinero=0.00,estado=True):
        datos = {
            "id_cuenta": self.idCuenta,
            "tipo": tipo,
            "dinero": dinero,
            "estado": estado
        }

        resultado = self.movimientoModel.registrar(datos)

        if resultado and estado:
            self.cuentaModel.retirarSaldo(self.idCuenta, self.dineroSacar)

        else:
            print("no se pudo sacar")

        self.moduloCuenta.cargarDatosColumnas()