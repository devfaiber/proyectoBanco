from models.cuentaModel import CuentaModel
from models.clienteModel import ClienteModel
import re
import wx
from wx import xrc

CUENTAS_COD = {"CC":"Cuenta Corriente","CV":"Cuenta Vivienda","FI":"Fondo Inversion"}
EXPRESION_NUMERO = r'([0-9]+|([0-9]+)+(\.[0-9]{1,3}))$'

class EditarCuentaFrame(wx.Frame):
    def __init__(self, moduloCuenta, id):
        super(EditarCuentaFrame, self).__init__()


        self.moduloCuenta = moduloCuenta
        self.cuentaModel = CuentaModel()
        self.idCuenta = id
        self.datosDB = self.cuentaModel.getOne(id=self.idCuenta)

        self.resp = xrc.XmlResource("../views/editar_cuenta.xrc")
        self.frame = self.resp.LoadFrame(None, "editCuentaFrame")
        self.panel = xrc.XRCCTRL(self.frame, "panelEditCuenta")

        self.tipo = xrc.XRCCTRL(self.panel, "tipo")
        self.panel.Bind(wx.EVT_COMBOBOX, self.comisionarAutomaticas, self.tipo)

        self.numero = xrc.XRCCTRL(self.panel, "numero")
        self.saldo = xrc.XRCCTRL(self.panel, "saldo")
        self.interes = xrc.XRCCTRL(self.panel, "interes")
        self.comision = xrc.XRCCTRL(self.panel, "comision")
        self.puntos = xrc.XRCCTRL(self.panel, "puntos")
        self.estado = xrc.XRCCTRL(self.panel, "estado")
        self.id_cliente = xrc.XRCCTRL(self.panel, "id_cliente")



        self.btnRegistrar = xrc.XRCCTRL(self.panel, "btnEditCuenta")
        self.frame.Bind(wx.EVT_BUTTON, self.actualizarCuenta, self.btnRegistrar)
        self.cargarDatosCuenta()

        self.frame.Show()

    def comisionarAutomaticas(self, evento):

        widget = evento.EventObject

        if widget.GetValue() == "CC":
            self.interes.SetValue("0.1")

        elif widget.GetValue() == "CV":
            self.interes.SetValue("0.2")

        else:
            self.interes.SetValue("0.34")


    def getDatos(self):
        datos = {
            #"tipo": self.tipo.GetValue(),
            "tipo": self.datosDB.tipo,
            "numero": self.numero.GetValue(),
            "saldo": self.saldo.GetValue(),
            "interes": self.interes.GetValue(),
            "comision": self.comision.GetValue(),
            "puntos": self.puntos.GetValue(),
            "estado": self.estado.GetStringSelection(),
            "id_cliente": self.id_cliente.GetStringSelection()

        }
        return datos


    def cargarDatosCuenta(self):

        if not self.datosDB is None:

            self.tipo.SetValue(CUENTAS_COD[self.datosDB.tipo])
            self.numero.SetValue(self.datosDB.numero)
            self.saldo.SetValue(str(self.datosDB.saldo))
            self.interes.SetValue(str(self.datosDB.interes))
            self.comision.SetValue(str(self.datosDB.comision))
            self.puntos.SetValue(str(self.datosDB.puntos))
            self.estado.SetStringSelection(self.datosDB.estado)

            #establece la seleccion en el combox para cliente viculado
            self.refrescarIdCliente()
            self.id_cliente.SetStringSelection(str(self.datosDB.id_cliente))

        else:
            self.frame.Close()


    def actualizarCuenta(self, evento):
        datos = self.getDatos()

        if datos["id_cliente"] == "None" or datos["id_cliente"] == "":
            datos["id_cliente"] = None


        if not self.validarDatos(datos):
            return False


        if self.cuentaModel.buscar(id=self.idCuenta):
            resultado = self.cuentaModel.editar(self.idCuenta, datos)
            if resultado:
                wx.MessageBox("el cuenta fue modificada exitosamente","CUENTA EDITADO",wx.OK|wx.ICON_INFORMATION)
                self.moduloCuenta.cargarDatosColumnas()
                self.frame.Close()
            else:
                wx.MessageBox("Lo sentimos al parecer el cliente con el id especificado no existe", "ERROR CLIENTE NO ENCONTRADO", wx.OK | wx.ICON_INFORMATION)
        else:
            wx.MessageBox("Lo sentimos el cliente no se puede editar Intente mas tarde","ERROR",wx.OK|wx.ICON_ERROR)



    def validarDatos(self, datos):

        if datos["comision"] == "":
            wx.MessageBox("la comision no puede quedar vacia", "ERROR DE COMISION", wx.OK | wx.ICON_ERROR)
            return False
        elif not re.match(EXPRESION_NUMERO,datos["comision"]):
            wx.MessageBox("Porfavor la comision no puede ser cadena de texto o valores negativos","ERROR DE COMISION",wx.OK|wx.ICON_ERROR)
            return False
        else:
            return True

    def refrescarIdCliente(self):
        arr_id = ["None"]
        for item in ClienteModel().getAll():
            arr_id.append(str(item.id))

        self.id_cliente.SetItems(arr_id)


