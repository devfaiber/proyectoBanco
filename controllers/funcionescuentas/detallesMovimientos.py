import datetime
import wx
from wx import xrc
from models.cuentaModel import CuentaModel
from models.movimientoModel import MovimientoModel
class DetalleMovimientoFrame(wx.Frame):
    def __init__(self, id):
        self.idCuenta = id
        self.cuentaModel = CuentaModel()
        self.movimientoModel = MovimientoModel()

        self.recurso = xrc.XmlResource("../views/detalles_movimientos.xrc")
        self.frame = self.recurso.LoadFrame(None, "revisionFrame")
        self.panel = xrc.XRCCTRL(self.frame, "panel_revision")

        self.campoMes = xrc.XRCCTRL(self.panel, "mes")


        self.btnBuscar = xrc.XRCCTRL(self.panel, "btn_buscar")
        self.frame.Bind(wx.EVT_BUTTON,self.consultarMovimientos, self.btnBuscar)

        self.listCtr = xrc.XRCCTRL(self.panel, "lista_movimientos")

        self.cargarColumnas()
        self.cargarDatosColumnas()

        self.frame.Show()

    def cargarColumnas(self):
        self.listCtr.InsertColumn(0, "id", format=wx.LIST_FORMAT_CENTER, width=wx.LIST_AUTOSIZE)
        self.listCtr.InsertColumn(1, "cuenta", format=wx.LIST_FORMAT_CENTER, width=wx.LIST_AUTOSIZE)
        self.listCtr.InsertColumn(2, "tipo", format=wx.LIST_FORMAT_CENTER, width=wx.LIST_AUTOSIZE)
        self.listCtr.InsertColumn(3, "dinero", format=wx.LIST_FORMAT_CENTER, width=120)
        self.listCtr.InsertColumn(4, "fecha de transaccion", format=wx.LIST_FORMAT_CENTER, width=200)
        self.listCtr.InsertColumn(5, "estado", format=wx.LIST_FORMAT_CENTER, width=wx.LIST_AUTOSIZE)

    def consultarMovimientos(self, evento):
        resultado = self.movimientoModel.getMovimiento(id_cuenta=self.idCuenta)
        filtro = self.filtrarPorFecha(resultado)

        if filtro:
           self.cargarDatosColumnas(filtro)
        else:
            wx.MessageBox("No se encontro movimientos en este mes", "NO HAY MOVIMIENTOS", wx.OK | wx.ICON_WARNING)



    def filtrarPorFecha(self, datos):
        mes = self.campoMes.GetValue()
        result = list(filter(lambda elem: elem.fecha.strftime("%m") == mes, datos))

        return result

    def cargarDatosColumnas(self, listadoMovimiento=None):
        self.listCtr.DeleteAllItems()

        if listadoMovimiento is None:
            listadoMovimiento = self.movimientoModel.getMovimiento(id_cuenta=self.idCuenta)

        for items in listadoMovimiento:
            self.listCtr.Append([items.id, items.id_cuenta, items.tipo, items.dinero, items.fecha, items.estado])
