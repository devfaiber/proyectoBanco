import wx
from models.cuentaModel import CuentaModel
from wx import xrc


class RevisionFrame(wx.Frame):

    def __init__(self, id):
        self.idCuenta = id
        self.cuentaModel = CuentaModel()

        super(RevisionFrame, self).__init__()

        self.recurso = xrc.XmlResource("../views/revision_mensual.xrc")
        self.frame = self.recurso.LoadFrame(None, "revisionFrame")
        self.panel = xrc.XRCCTRL(self.frame, "panel_revision")
        self.txtInteres = xrc.XRCCTRL(self.panel, "interes")
        self.txtComision = xrc.XRCCTRL(self.panel, "comision")
        self.txtSaldoTotal = xrc.XRCCTRL(self.panel, "saldo_total")
        self.txtTotalRevision = xrc.XRCCTRL(self.panel, "revision_total")

        self.setData()

        self.frame.Show()

    def setData(self):
        resultado = self.cuentaModel.getOne(id=self.idCuenta)

        if resultado is None:
            return False

        if resultado.tipo == "CV":
            self.txtComision.SetLabel("Comision: N/A")
            resultado.comision = 0.0
        else:
            self.txtComision.SetLabel("Comision: " + str(int(resultado.comision * 100)) + "%")

        self.txtInteres.SetLabel("Intereses: "+str(int(resultado.interes*100))+"%")

        self.txtSaldoTotal.SetLabel("Saldo Total: "+str(resultado.saldo)+" €")

        self.txtTotalRevision.SetLabel(self.getRevision(resultado))

    def getRevision(self, datos):

        interes = datos.interes
        print(interes)
        comision = datos.comision
        saldo = float(datos.saldo)

        ganacia = saldo*interes

        print(ganacia)
        perdida = (saldo+ganacia)*comision
        print(perdida)

        total = (saldo+ganacia) - (perdida)
        total = "%.2f"%total
        return str(total)+" €"

