import wx
from wx import xrc
from models.loginModel import LoginModel
from controllers.main import MainFrame

class LoginFrame(wx.Frame):
    def __init__(self, *args, **kw):
        super(LoginFrame, self).__init__(*args, **kw)
        self.loginModel = LoginModel()
        self.mainFrame = None

        self.interfaz = xrc.XmlResource("../views/login.xrc")
        self.frame = self.interfaz.LoadFrame(None, "loginFrame")
        self.panel = xrc.XRCCTRL(self.frame, "panelLogin")
        self.username = xrc.XRCCTRL(self.panel, "inputUsername")
        self.password = xrc.XRCCTRL(self.panel, "inputPassword")
        self.botonAcceder = xrc.XRCCTRL(self.panel, "btnAcceder")
        self.botonSalir = xrc.XRCCTRL(self.panel, "btnSalir")
        self.frame.Bind(wx.EVT_BUTTON, lambda evento: self.frame.Close(), self.botonSalir)
        self.frame.Bind(wx.EVT_BUTTON, self.iniciarLoguin, self.botonAcceder)

        self.frame.Show()


    def iniciarLoguin(self, evento):
        password = self.password.GetValue()
        username = self.username.GetValue()
        if self.validarLoguin(username, password):
            result = self.loginModel.getLoguin(username=username, password=password)

            if result:
                self.mainFrame = MainFrame(self)
                self.limpiarCampos()
                self.frame.Hide()
            else:
                wx.MessageBox("Usuario y contraseña erroneo verifique su informacion","Usuario Incorrecto",wx.OK|wx.ICON_ERROR)



    def validarLoguin(self, username, password):
        if password and username:
            return True
        else:
            return False

    def limpiarCampos(self):
        self.username.SetValue("")
        self.password.SetValue("")

if __name__ == '__main__':
    app = wx.App()
    login = LoginFrame()

    app.MainLoop()