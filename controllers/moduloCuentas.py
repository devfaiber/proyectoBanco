import random
import wx
import re
from wx import xrc
from models.cuentaModel import CuentaModel
from models.clienteModel import ClienteModel
from controllers.funcionescuentas.editarCuentaFrame import EditarCuentaFrame
from controllers.funcionescuentas.consultarCuentaFrame import ConsultarFrame
from controllers.funcionescuentas.depositoCuentaFrame import DepositoFrame
from controllers.funcionescuentas.retirarCuentaFrame import RetiroFrame
from controllers.funcionescuentas.detallesMovimientos import DetalleMovimientoFrame
from controllers.funcionescuentas.revisionCuentaFrame import RevisionFrame


EXPRESION_NUMERO = r'([0-9]+|([0-9]+)+(\.[0-9]{1,3}))$'
CUENTAS = {"Cuenta Vivienda": "CV", "Cuenta Corriente": "CC","Fondo Inversion": "FI"}

ELIMINAR_CUENTA = "Eliminar cuenta"
EDITAR_CUENTA = "Editar cuenta"
CONSULTAR_CUENTA = "Consultar cuenta"
RETIRAR_CUENTA = "Retirar dinero"
DEPOSITAR_CUENTA = "Depositar dinero"
DETALLE_MOVIMIENTO = "Ver movimientos"
REVISAR_CUENTA = "Revision mensual"

CUENTA_ACTIVA = "activa"
CUENTA_DESACTIVA = "desactiva"
CUENTA_BLOQUEADA = "bloqueada"

CUENTA_FI = "FI"
CUENTA_CC = "CC"
CUENTA_CV = "CV"

class CuentaModule():
    def __init__(self, panel):
        self.panel = panel
        self.panelAddCuenta = xrc.XRCCTRL(self.panel, "panelAddCuenta")
        self.listCtr = xrc.XRCCTRL(self.panel, "listaCuentas")

        self.cuentaSelected = None
        self.cuentaModel = CuentaModel()
        self.listaCuentas = None
        self.listaClientes = None

        #cargar capos
        self.getCampos()

        #menu 1 de cuenta
        self.btnAgregar = xrc.XRCCTRL(self.panelAddCuenta, "btnAddCuenta")
        self.panelAddCuenta.Bind(wx.EVT_BUTTON, self.crearCuenta, self.btnAgregar)
        self.btnRestablecer = xrc.XRCCTRL(self.panelAddCuenta, "btnRestablecer")
        self.panelAddCuenta.Bind(wx.EVT_BUTTON, lambda evt: self.restablecerCamposCrear(), self.btnRestablecer)

        self.panelAddCuenta.Bind(wx.EVT_COMBOBOX, self.comisionarAutomaticas, self.tipo)


    def getCampos(self):
        self.tipo = xrc.XRCCTRL(self.panelAddCuenta, "tipo")
        self.numero = xrc.XRCCTRL(self.panelAddCuenta, "numero")
        self.numero.SetValue(self.generarNumeroCuenta())

        self.saldo = xrc.XRCCTRL(self.panelAddCuenta, "saldo")
        self.interes = xrc.XRCCTRL(self.panelAddCuenta, "interes")
        self.comision = xrc.XRCCTRL(self.panelAddCuenta, "comision")
        self.puntos = xrc.XRCCTRL(self.panelAddCuenta, "puntos")
        self.estado = xrc.XRCCTRL(self.panelAddCuenta, "estado")
        self.id_cliente = xrc.XRCCTRL(self.panelAddCuenta, "id_cliente")

        #agregar los item al combox
        self.refrescarIdCliente()


    def comisionarAutomaticas(self, evento):

        widget = evento.EventObject.GetValue()
        tipo_cuenta = CUENTAS[widget]

        if tipo_cuenta == CUENTA_CC:
            self.interes.SetValue("0.1")

        elif tipo_cuenta == CUENTA_CV:
            self.interes.SetValue("0.2")

        else:
            self.interes.SetValue("0.34")

    def cargarColumnas(self):
        self.listCtr.InsertColumn(0, "id", format=wx.LIST_FORMAT_CENTER, width=wx.LIST_AUTOSIZE)
        self.listCtr.InsertColumn(1, "tipo", format=wx.LIST_FORMAT_CENTER, width=wx.LIST_AUTOSIZE)
        self.listCtr.InsertColumn(2, "numero", format=wx.LIST_FORMAT_CENTER, width=wx.LIST_AUTOSIZE)
        self.listCtr.InsertColumn(3, "saldo", format=wx.LIST_FORMAT_CENTER, width=wx.LIST_AUTOSIZE)
        self.listCtr.InsertColumn(4, "interes", format=wx.LIST_FORMAT_CENTER, width=wx.LIST_AUTOSIZE)
        self.listCtr.InsertColumn(5, "comision", format=wx.LIST_FORMAT_CENTER, width=wx.LIST_AUTOSIZE)
        self.listCtr.InsertColumn(6, "puntos", format=wx.LIST_FORMAT_CENTER, width=wx.LIST_AUTOSIZE)
        self.listCtr.InsertColumn(7, "estado", format=wx.LIST_FORMAT_CENTER, width=wx.LIST_AUTOSIZE)
        self.listCtr.InsertColumn(8, "cliente", format=wx.LIST_FORMAT_CENTER, width=wx.LIST_AUTOSIZE)


    def cargarDatosColumnas(self):
        self.listaCuentas = self.cuentaModel.getAll()
        self.listCtr.DeleteAllItems()

        for cuenta in self.listaCuentas:

            datos = [
                cuenta.id,
                cuenta.tipo,
                cuenta.numero,
                cuenta.saldo,
                str(int(cuenta.interes*100))+"%",
                str(int(cuenta.comision*100))+"%",
                cuenta.puntos,
                cuenta.estado,
                cuenta.id_cliente
            ]
            self.listCtr.Append(datos)


    def selectCuenta(self, evento):
        item_actual = evento.GetIndex()
        self.cuentaSelected = self.listaCuentas[item_actual]
        self.createMenu()


    def createMenu(self):
        menu = wx.Menu()

        id_consultar_dinero = wx.NewId()
        id_consignar_dinero = wx.NewId()
        id_retirar_dinero = wx.NewId()
        id_editar_cuenta = wx.NewId()
        id_eliminar_cuenta = wx.NewId()
        id_movimiento_cuenta = wx.NewId()
        id_revisar_cuenta = wx.NewId()

        menu.Append(id_consultar_dinero, CONSULTAR_CUENTA)
        menu.Append(id_consignar_dinero, DEPOSITAR_CUENTA)
        menu.Append(id_retirar_dinero, RETIRAR_CUENTA)
        menu.Append(id_editar_cuenta, EDITAR_CUENTA)
        menu.Append(id_eliminar_cuenta, ELIMINAR_CUENTA)
        menu.Append(id_movimiento_cuenta, DETALLE_MOVIMIENTO)
        menu.Append(id_revisar_cuenta, REVISAR_CUENTA)

        self.listCtr.Bind(wx.EVT_MENU, self.menuPOPU, id=id_consignar_dinero)
        self.listCtr.Bind(wx.EVT_MENU, self.menuPOPU, id=id_retirar_dinero)
        self.listCtr.Bind(wx.EVT_MENU, self.menuPOPU, id=id_consultar_dinero)
        #nuevos
        self.listCtr.Bind(wx.EVT_MENU, self.menuPOPU, id=id_editar_cuenta)
        self.listCtr.Bind(wx.EVT_MENU, self.menuPOPU, id=id_eliminar_cuenta)
        self.listCtr.Bind(wx.EVT_MENU, self.menuPOPU, id=id_movimiento_cuenta)
        self.listCtr.Bind(wx.EVT_MENU, self.menuPOPU, id=id_revisar_cuenta)

        self.listCtr.PopupMenu(menu)
        menu.Destroy()

    def menuPOPU(self, evento):
        id_item = evento.GetId()
        menu = evento.GetEventObject()
        menu_item = menu.FindItemById(id_item)

        if menu_item.GetLabel() == CONSULTAR_CUENTA:
            frameConsultar = ConsultarFrame(self.cuentaSelected.id)
            frameConsultar.cargarDatosCuentas()

        elif menu_item.GetLabel() == DEPOSITAR_CUENTA:
            self.depositarCuenta()
        elif menu_item.GetLabel() == RETIRAR_CUENTA:
            self.retirarCuenta()
        elif menu_item.GetLabel() == EDITAR_CUENTA:
            self.editarCuenta()
        elif menu_item.GetLabel() == ELIMINAR_CUENTA:
            self.eliminarCuenta()
        elif menu_item.GetLabel() == DETALLE_MOVIMIENTO:
            self.verMovimientos()

        elif menu_item.GetLabel() == REVISAR_CUENTA:
            self.revisarCuenta()

    def crearCuenta(self, evento):
        datos = {
            "tipo": CUENTAS[self.tipo.GetValue()],
            "numero": self.numero.GetValue(),
            "saldo": self.saldo.GetValue(),
            "interes": self.interes.GetValue(),
            "comision": self.comision.GetValue(),
            "puntos": self.puntos.GetValue(),
            "estado": self.estado.GetStringSelection(),
            "id_cliente": self.id_cliente.GetValue()

        }
        if datos["id_cliente"] == "None" or datos["id_cliente"] == "":
            datos["id_cliente"] = None

        if not self.validarCuentaCrear(datos=datos):
            return False


        resultado = self.cuentaModel.crear(datos)

        if resultado:
            wx.MessageBox("Su cuenta se ha agregado satisfractoriamente", "CUENTA AGREGADA", wx.OK|wx.ICON_INFORMATION)
            self.cargarDatosColumnas()
            self.restablecerCamposCrear()
        else:
            wx.MessageBox("Su cuenta no pudo ser creada porfavor intente mas tarde, si este problema persiste comunicarse con el administrador", "ERROR AL AGREGAR", wx.OK | wx.ICON_INFORMATION)

    def validarCuentaCrear(self, datos):
        if not datos["saldo"] or not datos["comision"]:
            wx.MessageBox("faltan campos requeridos(*) por rellenar. porfavor rellene los campos faltantes","CAMPOS REQUERIDOS VACIOS", wx.OK | wx.ICON_WARNING)
            return False

        elif not re.match(EXPRESION_NUMERO, datos["saldo"]):
            wx.MessageBox("el saldo inicial no puede ser negativa y mucho menos una cadena de texto","SALDO ERRONEA", wx.OK | wx.ICON_WARNING)
            return False

        elif not re.match(EXPRESION_NUMERO, datos["comision"]):
            wx.MessageBox("la comision no puede ser negativa y mucho menos una cadena de texto","COMISION ERRONEA", wx.OK | wx.ICON_WARNING)
            return False

        return True

    def editarCuenta(self):
        EditarCuentaFrame(self, self.cuentaSelected.id)

    def eliminarCuenta(self):
        advertencia = "Recuerda que si lo haces eliminaras todo el historia de movimiento de esta cuenta. "
        confirmacion = wx.MessageBox(advertencia+"¿Estas seguro de eliminar esta cuenta? ", "CONFIRMACION", wx.YES_NO | wx.ICON_EXCLAMATION)

        if confirmacion == wx.YES:
            resultado = self.cuentaModel.delete(self.cuentaSelected.id)
            if resultado:
                wx.MessageBox("su cuenta se ha eliminado satisfactoriamente junto a sus movimientos", "CUENTA ELIMINADA", wx.OK | wx.ICON_INFORMATION)
                self.cargarDatosColumnas()

            else:
                wx.MessageBox("su cuenta no se ha podido eliminar intente mas tarde", "ERROR DE ELIMINACION", wx.OK | wx.ICON_ERROR)
        else:
            print("solicitud de eliminacion de cuenta cancelado")

    def verMovimientos(self):

        DetalleMovimientoFrame(id=self.cuentaSelected.id)


    def revisarCuenta(self):
        RevisionFrame(id=self.cuentaSelected.id)

    def restablecerCamposCrear(self):

        self.tipo.SetSelection(0)
        self.numero.SetValue(self.generarNumeroCuenta())
        self.saldo.SetValue("0.000")
        self.interes.SetValue("0.1")
        self.comision.SetValue("0.6")
        self.puntos.SetValue("0")
        self.estado.SetSelection(0)
        self.id_cliente.SetSelection(0)

    def refrescarIdCliente(self):
        arr_id = ["None"]
        for item in ClienteModel().getAll():
            arr_id.append(str(item.id))

        self.id_cliente.SetItems(arr_id)


    def generarNumeroCuenta(self):
        min = 000000000000
        max = 999999999999
        numero = 0

        while self.cuentaModel.buscar(numero=numero) or numero == 0:
            numero = random.randint(min, max)

        return str(numero)

    def retirarCuenta(self):

        if self.cuentaSelected.estado == CUENTA_ACTIVA:
            RetiroFrame(self, self.cuentaSelected.id)

        elif self.cuentaSelected.estado == CUENTA_DESACTIVA:
            wx.MessageBox("La cuenta no esta activa", "ERROR CUENTA DESACTIVADA",wx.OK|wx.ICON_WARNING)
        else:
            wx.MessageBox("La cuenta se ha bloquedo por retiro forzoso al sistema", "ERROR CUENTA BLOQUEADA", wx.OK | wx.ICON_WARNING)

    def depositarCuenta(self):
        if self.cuentaSelected.estado == CUENTA_DESACTIVA:
            wx.MessageBox("La cuenta no se encuentra activada para realizar acciones", "ERROR CUENTA DESACTIVADA", wx.OK | wx.ICON_WARNING)
        else:
            DepositoFrame(self, self.cuentaSelected.id)

