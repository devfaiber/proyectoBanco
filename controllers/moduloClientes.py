from models.clienteModel import ClienteModel
from controllers.funcionesclientes.editarClienteFrame import EditarCliente
import wx
import re
from wx import xrc

ELIMINAR_CLIENTE = "Eliminar"
EDITAR_CLIENTE = "Actualizar"
EXPRESION_REGULAR_EMAIL = r'[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(\.[a-z]{2,4})$'
EXPRESION_REGULAR_TELEFONO = r'([0-9]{8,12})$'
EXPRESION_REGULAR_DNI = r'([0-9]{8})+([a-zA-Z]{1})$'
class ClienteModule():

    def __init__(self, panel):
        self.panel = panel
        self.listCtr = xrc.XRCCTRL(self.panel, "listaClientes")

        self.panelAddCliente = xrc.XRCCTRL(self.panel, "panelAddCliente")
        self.clienteSelected = None
        self.clienteModel = ClienteModel()
        self.listaClientes = None

        self.editarFrame = None

        self.btnAgregar = xrc.XRCCTRL(self.panelAddCliente, "btnAddClientes")

    def cargarColumnas(self):
        self.listCtr.InsertColumn(0, "id", format=wx.LIST_FORMAT_CENTER, width=wx.LIST_AUTOSIZE)
        self.listCtr.InsertColumn(1, "dni", format=wx.LIST_FORMAT_CENTER, width=wx.LIST_AUTOSIZE)
        self.listCtr.InsertColumn(2, "nombre", format=wx.LIST_FORMAT_CENTER, width=wx.LIST_AUTOSIZE)
        self.listCtr.InsertColumn(3, "apellidos", format=wx.LIST_FORMAT_CENTER, width=wx.LIST_AUTOSIZE)
        self.listCtr.InsertColumn(4, "direccion", format=wx.LIST_FORMAT_CENTER, width=wx.LIST_AUTOSIZE)
        self.listCtr.InsertColumn(5, "email", format=wx.LIST_FORMAT_CENTER, width=wx.LIST_AUTOSIZE)
        self.listCtr.InsertColumn(6, "telefono", format=wx.LIST_FORMAT_CENTER, width=wx.LIST_AUTOSIZE)

    def cargarDatosColumnas(self):
        self.listaClientes = self.clienteModel.getAll()
        self.listCtr.DeleteAllItems()

        for cliente in self.listaClientes:
            self.listCtr.Append(
                [cliente.id, cliente.dni, cliente.nombre, cliente.apellidos, cliente.direccion, cliente.email, cliente.telefono])

    def selectCliente(self, evento):
        item_actual = evento.GetIndex()
        self.clienteSelected = self.listaClientes[item_actual]
        self.createMenu(evento)

    def createMenu(self,evento):
        menu = wx.Menu()
        id_item_editar = wx.NewId()
        id_item_eliminar = wx.NewId()

        menu.Append(id_item_editar, EDITAR_CLIENTE)
        menu.Append(id_item_eliminar, ELIMINAR_CLIENTE)

        self.listCtr.Bind(wx.EVT_MENU, self.menuPOPU, id=id_item_editar)
        self.listCtr.Bind(wx.EVT_MENU, self.menuPOPU, id=id_item_eliminar)
        self.listCtr.PopupMenu(menu)
        menu.Destroy()

    def menuPOPU(self, evento):
        id_item = evento.GetId()
        menu = evento.GetEventObject()
        menu_item = menu.FindItemById(id_item)

        if menu_item.GetLabel() == EDITAR_CLIENTE:
            self.editarFrame = EditarCliente(self, self.clienteSelected.id)

        elif menu_item.GetLabel() == ELIMINAR_CLIENTE:
            self.eliminarCliente()

    def eliminarCliente(self):
        confirmacion = wx.MessageBox("Estas seguro de eliminar este usuario", "CONFIRMACION", wx.YES_NO | wx.ICON_EXCLAMATION)
        if confirmacion == wx.YES:
            resultado = self.clienteModel.delete(self.clienteSelected.id)

            if resultado:
                wx.MessageBox("su cliente se ha eliminado satisfactoriamente", "CUENTA ELIMINADA", wx.OK | wx.ICON_WARNING)
                self.cargarDatosColumnas()

            else:
                msg = "su cliente no se pudo eliminar."
                msg += " verifique que no se este vinculado a una cuenta del banco"
                msg += " si lo esta desvincule de esa cuenta"
                wx.MessageBox(msg, "ERROR DE ELIMINACION", wx.OK | wx.ICON_ERROR)
        else:
            print("solicitud de eliminacion cliente cancelada a tiempo")

    def crearCliente(self, evento):

        datos = {
            "dni": xrc.XRCCTRL(self.panelAddCliente, "dni").GetValue(),
            "nombre": xrc.XRCCTRL(self.panelAddCliente, "nombre").GetValue(),
            "apellidos": xrc.XRCCTRL(self.panelAddCliente, "apellidos").GetValue(),
            "direccion": xrc.XRCCTRL(self.panelAddCliente, "direccion").GetValue(),
            "telefono": xrc.XRCCTRL(self.panelAddCliente, "telefono").GetValue(),
            "email": xrc.XRCCTRL(self.panelAddCliente, "email").GetValue()
        }

        if not self.validarCampos(datos):
            return False

        resultado = self.clienteModel.crear(datos)

        if resultado:
            wx.MessageBox("el cliente se ha agregado correctamente", "CLIENTE AGREGADO", wx.OK | wx.ICON_INFORMATION)
            self.cargarDatosColumnas()
            self.limpiarCampos()
        else:
            wx.MessageBox("error desconocido al agregar el cliente contacte al administrador","ERROR DESCONOCIDO",wx.OK|wx.ICON_ERROR)


    def validarCampos(self,datos):
        estado = True

        if datos["dni"]=="" or datos["nombre"]=="" or datos["apellidos"]=="" or datos["email"]=="":
            wx.MessageBox("faltan campos requeridos(*) por rellenar. porfavor rellene los campos faltantes","CAMPOS REQUERIDOS VACIOS",wx.OK|wx.ICON_WARNING)
            estado = False
        elif not self.isValidoDNI(dni=datos["dni"]):
            estado = False

        elif datos["telefono"] != "" and not re.match(EXPRESION_REGULAR_TELEFONO,datos["telefono"]):
            wx.MessageBox("El numero de telefono debe ser numerico y maximo de 8 a 12 digitos", "ERROR DE TELEFONO", wx.OK | wx.ICON_ERROR)
            estado = False

        elif not re.match(EXPRESION_REGULAR_EMAIL,datos["email"]):
            wx.MessageBox("El email invalido, coloca un email que sea valido", "ERROR EMAIL", wx.OK | wx.ICON_ERROR)
            estado = False

        elif self.clienteModel.buscar(dni=datos["dni"]):
            wx.MessageBox("El DNI introducido ya existe en la base de datos","ERROR DE DNI",wx.OK|wx.ICON_ERROR)
            estado = False

        elif self.clienteModel.buscar(email=datos["email"]):
            wx.MessageBox("El email ya existe prueba otro","ERROR DE AGREGACION", wx.OK | wx.ICON_ERROR)
            estado = False

        return estado

    def isValidoDNI(self, dni):

        if not re.match(EXPRESION_REGULAR_DNI,dni):
            wx.MessageBox("El DNI no posee 8 digitos y una letra al final. corregir y tener en cuenta el formato","ERROR DE DNI",wx.OK|wx.ICON_ERROR)
            return False

        else:
            return True


    def limpiarCampos(self):
        xrc.XRCCTRL(self.panelAddCliente, "dni").SetValue(""),
        xrc.XRCCTRL(self.panelAddCliente, "nombre").SetValue(""),
        xrc.XRCCTRL(self.panelAddCliente, "apellidos").SetValue(""),
        xrc.XRCCTRL(self.panelAddCliente, "direccion").SetValue(""),
        xrc.XRCCTRL(self.panelAddCliente, "telefono").SetValue(""),
        xrc.XRCCTRL(self.panelAddCliente, "email").SetValue("")