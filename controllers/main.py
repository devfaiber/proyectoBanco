
import wx
from wx import xrc

from controllers.moduloClientes import ClienteModule
from controllers.moduloCuentas import CuentaModule


class MainFrame(wx.Frame):
    def __init__(self, loginFrame=None):
        super(MainFrame, self).__init__()
        #instancia de los modulos
        self.clienteModule = None
        self.cuentaModule = None
        self.loginFrame = loginFrame
        #--


        self.res = xrc.XmlResource("../views/banco_movil.xrc")
        self.frame = self.res.LoadFrame(None, "mainFrame")

        self.listbook = xrc.XRCCTRL(self.frame, "listContent")
        self.panelHome = xrc.XRCCTRL(self.listbook, "panelHome")
        self.panelCliente = xrc.XRCCTRL(self.listbook, "panelClientes")
        self.panelCuenta = xrc.XRCCTRL(self.listbook, "panelCuentas")

        # cargar cada modulo en este espacio

        self.cargarModuloHome()
        self.cargarModuloCliente()
        self.cargarModuloCuenta()
        #------------------
        self.frame.Maximize(True)
        self.frame.Show()

    def cargarModuloHome(self):

        btnCliente = xrc.XRCCTRL(self.panelHome, "btnClientes")
        btnCuenta = xrc.XRCCTRL(self.panelHome, "btnCuentas")
        btnInfo = xrc.XRCCTRL(self.panelHome, "btnInfo")
        btnSalir = xrc.XRCCTRL(self.panelHome, "btnSalir")

        self.listbook.Bind(wx.EVT_BUTTON, lambda evt: self.listbook.SetSelection(1), btnCliente)
        self.listbook.Bind(wx.EVT_BUTTON, lambda evt: self.listbook.SetSelection(2), btnCuenta)
        self.listbook.Bind(wx.EVT_BUTTON, lambda evt: self.listbook.SetSelection(3), btnInfo)
        self.listbook.Bind(wx.EVT_BUTTON, self.volverLogin, btnSalir)

    def volverLogin(self, evento):
        if self.loginFrame:
            self.loginFrame.frame.Show()

        self.frame.Close()

    def cargarModuloCliente(self):
        self.clienteModule = ClienteModule(self.panelCliente)
        self.clienteModule.cargarColumnas()
        self.clienteModule.cargarDatosColumnas()

        # menu popup
        self.frame.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.clienteModule.selectCliente, self.clienteModule.listCtr)

        self.clienteModule.panelAddCliente.Bind(wx.EVT_BUTTON, self.crearCliente, self.clienteModule.btnAgregar)

    def crearCliente(self, event):
        self.clienteModule.crearCliente(event)

        self.cuentaModule.refrescarIdCliente()


    def cargarModuloCuenta(self):
        self.cuentaModule = CuentaModule(self.panelCuenta)

        self.cuentaModule.cargarColumnas()
        self.cuentaModule.cargarDatosColumnas()

        #menu popup
        self.frame.Bind(wx.EVT_LIST_ITEM_RIGHT_CLICK, self.cuentaModule.selectCuenta, self.cuentaModule.listCtr)

if __name__ == '__main__':
    app = wx.App()
    main = MainFrame()
    app.MainLoop()