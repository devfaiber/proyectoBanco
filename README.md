# proyectoBanco
el proyecto consiste en simular un banco usando una base de datos relacional como MYSQL que permite un usuario administrador ingresar con usuario y poder gestionar acciones como
- crear cuentas
- crear clientes
- vincular clientes a cuentas
- ver cliente, cuenta y revision mensual
- ver detalles del los movimientos de las cuentas
- eliminar cuentas
- eliminar clientes
- etc

### paquetes usados
- SQLAlchemy(ORM)
- PyMYSQL
- wxPython

### Requisitos para ejecutarlo
- Tener instalado mariadb o mysql
- Configurar el archivo settings que hay en el proyecto, y establecer la base de datos, usuario y contraseña para su acceso
- establecer el usuario que el sistema creara como admin
- tener python 3.7.x

**Nota:** el sistema automaticamente hace la gestion de la creacion de la base de datos y del usuario de acceso para el sistema simplemente en settings indicar el nombre de la base de datos del cual se guiara para crear, el usuario de la base de datos existente y la contraseña de acceso para que el sistema acceda a la base de datos.

### instalacion de wxpython

**NOTA:** algunas veces presenta error en la instalacion de wxpython ya que se hace el dificil asi que le dare unas posibles soluciones alternas
#### Window y MACOS
```
pip install -U wxpython
```
#### Linux
- sin pip para derivados de debian con gestor de paquetes apt-get
```
apt-get install python-wxgtk2.8
```
- con pip para cualquiera en especial Ubuntu
```
pip install -U \
    -f https://extras.wxpython.org/wxPython4/extras/linux/gtk3/ubuntu-16.04 \
    wxPython
```
mayor informacion en su [pagina oficial](https://www.wxpython.org/pages/downloads/) 
o tambien en la [wiki oficial](https://wiki.wxpython.org/How%20to%20install%20wxPython) de wxpython explican la instalacion para window como para diferentes linux

Espero que el sistema funcione para pruebas o otras cosas relacionadas, cualquier duda contacto a dev.faiber@gmail.com para poder dar paso a paso si no funciona

